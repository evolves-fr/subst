# Subst

*Inspired of Gettext*

Substitutes the values of template.

In normal operation mode, standard input is copied to standard output, with [Sprig functions](https://masterminds.github.io/sprig/).

## Credits (Maintainer alphabetical name order)
- [Masterminds - Sprig](https://github.com/Masterminds/sprig): Useful template functions for Go templates.
- [Joho - GoDotEnv](https://github.com/joho/godotenv): A Go port of Ruby's dotenv library (Loads environment variables from `.env`.)
- [Urfave - CLI](https://github.com/urfave/cli): A simple, fast, and fun package for building command line apps in Go

## License
[MIT License](LICENSE)

## Usage
```shell
subst < input > output
```

## Example

- Go to `tmp`
```shell
cd /tmp
```

- Create input file
```shell
echo 'Current pwd = {{ env "PWD" }}' > input.txt
```

- Execute `subst`
```shell
subst < input.txt > output.txt
```

- Output result
```shell
cat output.txt
```

`Current pwd = /tmp`

code_quality:
	golangci-lint run --issues-exit-code 0

build:
	goreleaser release --rm-dist --snapshot

release:
	goreleaser release --rm-dist

package main

import (
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
	"text/template"

	"github.com/Masterminds/sprig"
	"github.com/joho/godotenv"
	"github.com/urfave/cli/v2"
)

var (
	version string
	commit  string
	date    string
)

func main() {
	app := cli.App{
		Name:        "subst",
		Usage:       "Substitutes the values of template.",
		UsageText:   "subst < input > output",
		Description: `In normal operation mode, standard input is copied to standard output, with Sprig functions.`,
		Version:     fmt.Sprintf("%s (%s - %s)", version, commit, date),
		Copyright:   "EVOLVES™ - https://www.evolves.fr",
		Authors:     []*cli.Author{{Name: "Thomas FOURNIER", Email: "tfournier@evolves.fr"}},
		Flags: []cli.Flag{
			&cli.BoolFlag{
				Name:  "with-dotenv",
				Usage: "load dotenv file",
				Value: false,
			},
		},
		Action: action,
	}

	if err := app.Run(os.Args); err != nil {
		_, _ = fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}

func action(c *cli.Context) error {
	if c.Bool("with-dotenv") {
		if err := godotenv.Load(); err != nil {
			return err
		}
	}

	input, err := io.ReadAll(os.Stdin)
	if err != nil {
		return err
	}

	tpl, err := template.New("base").Funcs(functions()).Parse(string(input))
	if err != nil {
		return err
	}

	return tpl.Execute(os.Stdout, nil)
}

func functions() template.FuncMap {
	fn := sprig.TxtFuncMap()
	fn["toBool"] = toBool

	return fn
}

func toBool(v interface{}) bool {
	var value string

	switch strings.ToLower(fmt.Sprintf("%v", v)) {
	case "true", "on", "1":
		value = "true"
	case "false", "off", "0":
		value = "false"
	}

	result, err := strconv.ParseBool(value)
	if err != nil {
		return false
	}

	return result
}
